<?php

error_reporting(E_ALL);
class App{
    public $controller = "home";
    public $action = "index";
    public $params = [];
    
    
    function __construct($url){
        $url = $this->parseUrl();
        
        if(file_exists("../app/controllers/" . $url[0] .'controller.php'))
        {
            $this->controller = $url[0];
            unset($url[0]);
        }
        require_once("../app/controllers/" . $this->controller . "controller.php");
        
        $controllertype = $this->controller."Controller";
        $this->controller = new $controllertype;
        
        if(isset($url[1]))
        {
            // if(method_exists($this->controller, $url[1]))
            // {
                $this->action = $url[1];
                unset($url[1]);
            // }
            
        }
        
        $classaction = $this->getAction($controllertype, $this->action);
        
        if($classaction == null)
        {
           $classaction = "index";
        }
        
        $requesttype = $_SERVER["REQUEST_METHOD"];

        if($requesttype == "POST")
        {

          //check parameters
          $class = new ReflectionClass($this->controller);
          $action = $class->getMethod($classaction);
          $parameters = $action->getParameters();
          if($parameters != null)
          {
            foreach ($parameters as $param) {
              if(is_array($param))
              {
                if(isset($_POST[$param->name]))
                {
                  $this->params[] = $_POST[$param->name];
                }
                continue;
              }
              $paramtype = $param->getClass()->name;
              //bind post data to new object
              $objecttobind = $paramtype::bind($_POST[$param->name]);
              Model::ValidateObject($objecttobind);
              //clear and replace params with newly created object
              $this->params = [];
              $this->params[] = $objecttobind;
            }
          }
        }
        else
        {
          $this->params = $url ? array_values($url) : [];
        }
        call_user_func_array([$this->controller, $classaction], $this->params);  
    }
    
    private function getAction($controller, $action)
    {
      $requesttype = $_SERVER["REQUEST_METHOD"];
      
      $requestmap = [];
      $requestmap['GET'] = 'HttpGetAttribute';
      $requestmap['POST'] = 'HttpPostAttribute';
      foreach ($this->getActions($controller) as $actionname => $attributes) {
        foreach ($attributes as $key => $value) {
          if($key == $requestmap[$requesttype] && strtolower($value->actionname) == strtolower($action))
          {
            return $actionname;
          }
        } 
      }
    }
    
    private function getActions($controller)
    {
      $class = new ReflectionClass($controller);
      $actions = [];
      foreach ($class->getMethods() as $method) {
        $attributes = Attribute::getAttributes($method);
        if($attributes == null) continue;
        $actions[$method->name] = $attributes;
      }
      return $actions;
    }
    
    
    
    public function parseUrl()
    {
        if(isset($_GET["url"]))
        {
            return $url = explode('/', filter_var(rtrim($_GET['url'],'/'),FILTER_SANITIZE_URL));
        }
    }
}