<?php
  $driver = 'mysql';
	$host = 'localhost';
	$dbname = 'sample';
	$username = 'root';
	$password = '';
  
  try
  {
    $db = new Database("$driver:host=$host;dbname=$dbname;charset=utf8",$username);
  
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $db->tblprefix = "";
    $db->tblidentity = "id";
    
    Globals::$db = $db;  
  }
  catch(Exception $ex)
  {
    echo $ex->getMessage();
  }
  