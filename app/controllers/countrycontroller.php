<?php
    class CountryController extends Controller
    {
        private $_service;
        public function __construct()
        {
            $this->_service = $this->createservice("country");
        }
        
        /**
        *@HttpGet(Index)
        */
        public function index()
        {
            $countries = $this->_service->GetAllCountries();
            $this->view("country/index",$countries);
        }
        /**
        *@HttpGet(Create)
        */
        public function create()
        {
            $country = new Country;
            $this->view("country/create",$country);
        }
        
        /**
        *@HttpPost(Create)
        */
        public function createdata(Country $country)
        {
          //try to save the country to database
          if($this->_service->create($country))
          {
              //return to index if save was successful
              $this->redirect('country');
          }
          
          $this->view("country/create",$country);
        }
        
        public function partialcreate()
        {
          $country = new Country();
          $country->value = "PH";
          $this->partialview("country/partialcreate",$country);
        }
        
        /**
        *@HttpGet(Edit)
        */
        public function edit($id = '')
        {
          $country = new Country;
          if($id == '')
          {
              $this->view('shared/400'); //bad request
          }
          //check if entity exists
          $country = Country::find($id);
          
          if($country == null)
          {
              $this->view('shared/400'); //bad request
          }
          $this->view("country/edit",$country);
        }
        /**
        *@HttpPost(Edit)
        */
        public function editdata(Country $country)
        {
          //try to save the country to database
          
          if($this->_service->update($country))
          {
              //return to index if save was successful
              $this->redirect('country');
          }
          $this->view("country/edit",$country);
        }
        
        /**
        *@HttpGet(Delete)
        */
        public function delete($id = '')
        {
          $country = new Country;
          if($id == '')
          {
            $this->view('shared/400');
            return;
          }
          
          $country = Country::find($id);
          
          if($country == null)
          {
            $this->view('shared/400');
            return;
          }
          
          $this->view('country/delete', $country);
        }
        
        /**
        *@HttpPost(Delete)
        */
        public function deletedata(Country $country)
        {
          $country = Country::find($country->id);
          
          if($country == null)
          {
            $this->view('shared/400');
            return;
          }
          if($this->_service->delete($country))
          {
            $this->redirect('country');
            return;
          }
          $this->view('country/delete', $country);
        }
        
        /**
        *@HttpPost(AsyncDelete)
        */
        public function asyncdelete($id = '')
        {
          $country = new Country;
          if($id == '')
          {
            $this->view('shared/400');
            return;
          }
          
          $country = Country::find($id);
          
          if($country == null)
          {
            $this->view('shared/400');
            return;
          }
          if($this->_service->delete($country))
          {
            echo json_encode(['result'=>true]);
            return;
          }
        }
    }