<?php
    class Student extends Model
    {
        public $id;
        public $name;
        
        public $_validations = [
          'name' => [
            'maxlength' => 50,
            'required' => "Name is required!"
          ]
        ];
    }