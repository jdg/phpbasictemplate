<?php
    class Section extends Model
    {
        public $id;
        public $value;
        public $description;
        
        public $_validations = [
          'value' => [
            'maxlength' => 2,
            'required' => "Value is required!"
          ],
          'description' => [
            'maxlength' => 10,
            'required' => "Description is required!"
          ]
        ];
    }