<?php
    class Country extends Model
    {
        public $id;
        /**
        *@MaxLength(3)
        *@Required(Value is required)
        *@Display(Value)
        */
        public $value;
        /**
        *@MaxLength(10)
        *@Required(Description is required)
        *@Display(Description)
        */
        public $description;
    }