<?php
    class CountryService implements ICountryService
    {
        private $repository;
        private $unitofwork;
        
        public function __construct()
        {
            $this->unitofwork = new UnitOfWork();
            $this->repository = new CountryRepository($this->unitofwork->dbcontext);
        }
        
        public function Validate($entity)
        {
            return $entity->isvalid();
        }
        public function GetAllCountries()
        {
            return $this->repository->GetAll();
        }
        
        public function Create(Country $country)
        {
            if(!$this->Validate($country))
            {
                return false;
            }
            
            $this->repository->Create($country);
            $this->unitofwork->SaveChanges();
            
            return true;
        }
        
        public function Update(Country $country)
        {
          if(!$this->Validate($country))
          {
            return false;
          }
          $this->repository->Update($country);
          $this->unitofwork->SaveChanges();
          
          return true;
        }
        
        public function Delete(Country $country)
        {
          $this->repository->Delete($country);
          $this->unitofwork->SaveChanges();
          
          return true;
        }
    }
    
    interface ICountryService
    {
        public function GetAllCountries();
    }