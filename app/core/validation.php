<?php
  class Validation
  {
    public static function maxlength($entity, $rule, $property)
    {
      if(strlen($entity->$property) > $rule)
      {
        $entity->_state[$property] = "Maximum of $rule character(s) only!";
      }
    }
    
    public static function required($entity,$rule, $property)
    {
      if(!isset($entity->$property) || empty($entity->$property))
      {
        $entity->_state[$property] = $rule;
      }
    }
  }