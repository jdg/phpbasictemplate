<?php
  class MaxLengthAttribute extends Attribute implements IValidationAttribute
  {
    public $maxlength;
    
    public function __construct($maxlength)
    {
      $this->maxlength = $maxlength;
    }
    
    public function IsValid($object)
    {
      if(strlen($object) > $this->maxlength)
      {
        return "Please enter equal or less than $this->maxlength letters.";
      }
    }
  }