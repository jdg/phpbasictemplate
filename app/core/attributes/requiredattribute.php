<?php
	class RequiredAttribute extends Attribute implements IValidationAttribute
	{
		public $errormessage;

		public function __construct($errormessage)
		{
			$this->errormessage = $errormessage;
		}

		public function IsValid($object)
		{
      
      if(empty($object))
      {
        return $this->errormessage;  
      }
      return null;			
		}
	}