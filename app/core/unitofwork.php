<?php
  class UnitOfWork
  {
    public $dbcontext;
    public function __construct()
    {
      $this->dbcontext = new DbContext();
      Globals::$db->beginTransaction();
    }
    public function SaveChanges()
    {
      try
      {
        $this->dbcontext->SaveChanges();     
        Globals::$db->commit();
      }
      catch(Exception $ex)
      {
        //print error message
        echo $ex->getMessage();
        Globals::$db->rollBack();
      }
    }
  }