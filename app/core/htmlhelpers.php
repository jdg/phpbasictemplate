<?php
	class Html
	{
		public static function DisplayNameFor($object, $propertyname)
		{
			$classname = get_class($object);
	        $class = new ReflectionClass($classname);

	        $property = $class->getProperty($propertyname);
	        $attribute = Attribute::getAttribute($property,'DisplayAttribute');
	        if($attribute == null) return '';
	        return $attribute->name;
		}

		public static function LabelFor($object, $propertyname)
		{
			$displayname = Html::DisplayNameFor($object, $propertyname);
			$classname = get_class($object);
      return "<label class='uk-form-label' for='$classname"."[$propertyname]"."'>$displayname</label>";
		}

		public static function EditorFor($object, $propertyname)
		{
			$classname = strtolower(get_class($object));
			$value = Html::ValueFor($object, $propertyname);
      return "<input id='$classname"."[$propertyname]"."' type='text' name='$classname"."[$propertyname]"."' autocomplete='off' value='$value'/>";
		}

		public static function HiddenFor($object, $propertyname)
		{
			$value = Html::ValueFor($object, $propertyname);
			$classname = strtolower(get_class($object));
      return "<input id='$classname"."[$propertyname]"."' type='hidden' name='$classname"."[$propertyname]"."' value='$value'/>";
		}

		public static function ValueFor($object, $propertyname)
		{
			$classname = get_class($object);
      $class = new ReflectionClass($classname);
      return htmlspecialchars($object->$propertyname,ENT_QUOTES);
		}
    
    public static function ValidationFor($object, $propertyname)
    {
      if(!$object->_state) return;
      if(!array_key_exists($propertyname, $object->_state))
      {
        return;
      }
      if(!$object->_state[$propertyname])
      {
        return;
      }
      return "<div style='color:red'>".$object->_state[$propertyname]."</div>";
    }

	}