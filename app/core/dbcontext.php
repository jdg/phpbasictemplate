<?php
  class DbContext
  {
    private $commands;
    
    public function __construct()
    {
      $this->commands = [];
    }
    
    public function Add($entity)
    {
      $classname = get_class($entity);
      $tblname = strtolower(get_class($entity));
			$fields =	[];
			$params = [];
			$values = [];
      
			foreach ($classname::getcolumns() as $field) {
				if($field == Globals::$db->tblidentity) continue;
				$fields[] = "`$field`";
				$params[] = "?";
				$values[] = $entity->$field;
			}
			$fields = implode(",",$fields);
			$params = implode(",",$params);
			$query = "INSERT INTO `$tblname`($fields) VALUES($params)";
			$this->commands[] = array('query' => $query, 'params' => $values);
    }
    
    public function Remove($entity)
    {
      $identityfield = Globals::$db->tblidentity;
      $classname = get_class($entity);
      $tblname = strtolower(get_class($entity));
      $query = "DELETE FROM `$tblname` WHERE `$identityfield` = ?";
      $this->commands[] = array('query' => $query, 'params' => array($entity->$identityfield));
    }
    
    public function Update($entity)
    {
      $tblidentity = Globals::$db->tblidentity;
      $classname = get_class($entity);
      $tblname = strtolower(get_class($entity));
			$fields =	[];
			$values = [];
			foreach ($classname::getcolumns() as $field) {
				if($field == $tblidentity) continue;
				$fields[] = "`$field` = ?";
				$values[] = $entity->$field;
			}
			$fields = implode(",",$fields);
			
			$values[] = $entity->$tblidentity;
			
			$query = "UPDATE `$tblname` SET $fields WHERE $tblidentity = ?";
			$this->commands[] = array('query' => $query, 'params' => $values);
    }
    
    public function SaveChanges()
    {
      foreach ($this->commands as $cmd) {
        Globals::$db->execute($cmd['query'],$cmd['params']);
      }
    }
  }