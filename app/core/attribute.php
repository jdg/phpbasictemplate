<?php
  class Attribute
  {
    public static function getAttributes($object)
    {
      $attributes = [];
      $comment = $object->getDocComment();
      if($comment == null) return null;
      $commentlines = explode("\n", $comment);
      foreach ($commentlines as $commentline) {
        $atposition = strpos($commentline, "@");
        if(!$atposition)
        {
          continue;
        }
        $attr = rtrim(substr($commentline, $atposition + 1));
        $openparenpos = strpos($attr, "(");
        $attrname = substr($attr,0, $openparenpos);
        $classname = substr($attr,0, $openparenpos).'Attribute';
        $valueslen = strlen($attr) - strlen($attrname) - 2;
        $values = substr($attr, $openparenpos + 1, $valueslen);
        $rclass = new ReflectionClass($classname);
        $attributes[$classname] = $rclass->newInstanceArgs(explode(",",$values));
      } 
      return $attributes;
    }

    public static function getAttribute($object, $name)
    {
    	$attributes = Attribute::getAttributes($object);
    	if($attributes == null) return null;
    	return $attributes[$name];
    }
  }

  interface IValidationAttribute
  {
    public function IsValid($object);
  }