<?php
  class PartialView
  {
    private $viewname;
    public function __construct($viewname)
    {
      $this->viewname = $viewname;
    }
    public function __toString()
    {
      $path = "../app/views/".$this->viewname.".php";
      return renderPhpToString($path);
    }
    
    function renderPhpToString($file, $vars=null)
    {
        if (is_array($vars) && !empty($vars)) {
            extract($vars);
        }
        ob_start();
        include $file;
        return ob_get_clean();
    }
  }