<?php
    class Controller{
        public $showsidebar = true;
        public function view($viewname,$model = [],$viewdata = [])
        {
            $_htmlsections["body"] = "../app/views/".$viewname.".php";
            if(!isset($_pagetitle))
            {
                $_pagetitle = "";
            }
            $_sidebar = $this->showsidebar;
            require_once("../app/views/shared/layout.php");
        }
        
        public function partialview($viewname,$model = [],$viewdata = [])
        {
          $path = "../app/views/".$viewname.".php";
          require_once($path);
        }
        
        public function redirect($viewname)
        {
            header("Location: /phpbasictemplate/" . $viewname);
            exit;
        }
        
        public function createservice($servicename)
        {
            require_once("../app/services/".$servicename."service.php");
            $s = $servicename."service";
            return new $s;
        }
        
        public function model($modelname)
        {
            require_once("../app/models/".$modelname.".php");
            return new $modelname;
        }
    }