<?php
    class Model
    {
      public $_state;
      
      public static function find($recid)
      {
        $classname = get_called_class();
        $return = new $classname;
        $idname = Globals::$db->tblidentity;
        $query = "SELECT * FROM `$classname` WHERE `$idname`=?";
        $rs = Globals::$db->querySingle($query,array($recid));
        if(!$rs) return null;
        // $vars = array_keys(get_class_vars($classname));
        foreach ($classname::getcolumns() as $varname) {
          if(substr($varname,0,1) == "_")
          {
            continue;
          }
          $return->$varname = $rs[$varname]; 
        }
        return $return;
      }
      
      public static function findby($database, $field, $value)
      {
        $classname = get_called_class();
        $return = new $classname;
        $query = "SELECT * FROM tbl_$classname WHERE `$field`=?";
        $rs = $database->querySingle($query,array($value));
        if(!$rs) return null;
        foreach ($classname::getcolumns() as $varname) {
          $return->$varname = $rs[$varname]; 
        }
        
        return $return;
      }
      public static function createX($array_values)
          {
              
          }
      public static function delete($db, $recid)
      {
        $classname = get_called_class();
        $query = "DELETE FROM `$db->tblprefix$classname` WHERE `$db->tblidentity` = ?";
        $db->execute($query,array($recid));
      }
      
      public static function all($filter = '', $params = [])
      {
        $return = [];
        $classname = strtolower(get_called_class());
        $query = "SELECT * FROM `$classname` $filter";
        $rs = Globals::$db->query($query,$params);
        foreach ($rs as $key => $value) {
          $instance = new $classname;
          foreach ($classname::getcolumns() as $varname) {
            $instance->$varname = $value[$varname]; 
          }
          $return[] = $instance;
        }
        return $return;
      }
      public static function bind($array_values)
      {
        $classname = get_called_class();
        $return = new $classname;
        // $booleans = $return->getboolvars();
        $members = array_keys(get_class_vars($classname));
        foreach ($array_values as $key => $value) {
          if(in_array($key, $members))
          {
            $return->$key = $value;
            // if(in_array($key, $booleans))
            // {
            // 	$return->$key = $return->$key == 'on' ? '1' : '0';
            // }
          }
        }
        
        return $return;
      }
      
      public static function getcolumns()
      {
        $classname = get_called_class();
        $vars = array_keys(get_class_vars($classname));
        $returnarray = [];
        foreach ($vars as $var) {
          if(substr($var,0,1) == "_")
          {
            continue;
          }
          $returnarray[] = $var;
        }
        return $returnarray;
      }
      
      public function isvalid()
      {
        return !count($this->_state) > 0;
      }
      
      public static function ValidateObject($object)
      {
        $modelstate = [];
        $classname = get_class($object);
        $class = new ReflectionClass($classname);

        $properties = $class->getProperties();
        foreach ($properties as $property) {
          $attributes = Attribute::getAttributes($property);
          if($attributes == null) continue;
          foreach ($attributes as $attribname => $attribute) {
            $rclass = new ReflectionClass($attribname);
            if($rclass->implementsInterface('IValidationAttribute'))
            {
              $propertyname = $property->name;
              $isvalid = $attribute->IsValid($object->$propertyname);
              if($isvalid != null)
              {
                $modelstate[$property->name] = $isvalid;
              }
            }
          }
        }
        $object->_state = $modelstate;
      }
      
    }