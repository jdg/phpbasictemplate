<?php
  class Database extends PDO
	{
		public $tblprefix;
		public $tblidentity;
		public function query($query,$params = [], $mode = PDO::FETCH_ASSOC)
		{
			$stmt = $this->prepare($query);
			$stmt->execute($params);
			$rs = $stmt->fetchAll($mode);
			return $rs;
		}
		
		public function querySingle($query,$params = [], $mode = PDO::FETCH_ASSOC)
		{
			$stmt = $this->prepare($query);
			$stmt->execute($params);
			$rs = $stmt->fetch($mode);
			return $rs;
		}
		
		public function queryColumn($query, $params)
		{
			$stmt = $this->prepare($query);
			$stmt->execute($params);
			$rs = $stmt->fetchColumn();
			return $rs;
		}
		
		public function execute($query,$params = [])
		{
			$stmt = $this->prepare($query);
			$stmt->execute($params);
		}
		
		public function getreader($query,$params = [])
		{
			$stmt = $this->prepare($query);
			$stmt->execute($params);
			return $stmt;
		}
		
		public function insert($object)
		{
			$tblname = $this->tblprefix.get_class($object);
			$fields =	[];
			$params = [];
			$values = [];
			foreach (array_keys(get_class_vars(get_class($object))) as $field) {
				if($field == "recid") continue;
				$fields[] = "`$field`";
				$params[] = "?";
				$values[] = $object->$field;
			} 
			$fields = implode(",",$fields);
			$params = implode(",",$params);
			$query = "INSERT INTO `$tblname`($fields) VALUES($params)";
			$this->execute($query,$values);
		}
		public function update($object)
		{
			$tblname = $this->tblprefix.get_class($object);
			$fields =	[];
			$values = [];
			foreach (array_keys(get_class_vars(get_class($object))) as $field) {
				if($field == "recid") continue;
				$fields[] = "`$field` = ?";
				$values[] = $object->$field;
			}
			$fields = implode(",",$fields);
			
			$values[] = $object->recid;
			
			$query = "UPDATE `$tblname` SET $fields WHERE recid = ?";
			$this->execute($query,$values);
		}
	}