<a href="/phpbasictemplate/country">Back to List</a>
<form action="/phpbasictemplate/country/edit" method="post" class="uk-form uk-form-horizontal">
    <?=Html::HiddenFor($model,'id')?>
    <div class="uk-form-row">
    <?=Html::LabelFor($model,'value')?>
    <div class="uk-form-controls">
      <?=Html::EditorFor($model, 'value')?>
      <?=Html::ValidationFor($model, 'value')?>
    </div>
  </div> 
  <div class="uk-form-row">
    <?=Html::LabelFor($model,'description')?>
    <div class="uk-form-controls">
      <?=Html::EditorFor($model, 'description')?>
      <?=Html::ValidationFor($model, 'description')?>
    </div>
  </div> 
  <div class="uk-form-row">
    <button type='submit' class="uk-button uk-button-success">Save</button>
  </div>
</form>