<form action="/phpbasictemplate/country/create" method="post" class="uk-form uk-form-horizontal">
  <div class="uk-form-row">
    <label for="txt_name" class="uk-form-label">Value</label>
    <div class="uk-form-controls">
      <input id="txt_name" type="text" name="value" value="<?=$model->value?>"> <?=$model->getValidationMsg('value')?>  
    </div>
  </div> 
  <div class="uk-form-row">
    <label for="txt_description" class="uk-form-label">Description</label>
    <div class="uk-form-controls">
      <input id="txt_description" type="text" name="description" value="<?=$model->description?>"> <?=$model->getValidationMsg('description')?>  
    </div>
  </div>
    <button type='submit' class="uk-button uk-button-success">Save</button>
</form>