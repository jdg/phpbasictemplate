<h2>Countries</h2>
<div class="app-data-table">
  
  <ul class="uk-subnav">
    <li>
      <a href="/phpbasictemplate/country/create"><i class="uk-icon-plus"></i> Create New</a>
      <button id="btn_create" type="button" class="uk-button">Create New (Modal)</button>
      <li data-uk-dropdown="{mode:'click'}">
        <a href="#">More <i class="uk-icon-caret-down"></i></a>
        <div class="uk-dropdown uk-dropdown-small">
            <ul class="uk-nav uk-nav-dropdown">
                <li><a href="">Item</a></li>
                <li><a href="">Another Item</a></li>
            </ul>
        </div>
    </li>    
    </li>
  </ul>
  <table class="uk-table">
    <thead>
      <tr>
        <th>Value</th>
        <th>Description</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($model as $country):?>
        <tr id="row_<?=$country->id?>">
          <td><?=Html::ValueFor($country, 'value')?></td>
          <td><?=Html::ValueFor($country, 'description')?></td>
          <td class="app-data-col-action">
            <a href="/phpbasictemplate/country/edit/<?=$country->id?>"><i class="uk-icon-pencil"></i>Edit</a>
            <a href="/phpbasictemplate/country/delete/<?=$country->id?>"><i class="uk-icon-remove"></i>Delete</a>
            <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                <a><i class="uk-icon-remove"></i>Delete Async</a>
                <div class="uk-dropdown">
                  <div style="margin-bottom:5px;">
                    <strong>Confirm Delete</strong>
                  </div>
                  <div class="uk-button-group">
                    <button class="uk-button uk-button-danger" onclick="removecountry(<?=$country->id?>)">OK</button>
                    <button class="uk-button">CANCEL</button>  
                  </div>
                </div>
            </div>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>      
</div>


<script>
  (function(){
    $("#btn_create").click(function(){
      $.ajax({
        url : "/phpbasictemplate/country/partialcreate",
        dataType: "html",
        success: function(response)
        {
          console.log(response);
        }
      });
    });
  })()
  
  function removecountry(rowid)
  {
    $.ajax({
      method: "post",
      url: '/phpbasictemplate/country/asyncdelete',
      dataType: "json",
      data: {id:rowid},
      success: function(response)
      {
        if(response.result)
        {
          $("#row_" + rowid).fadeOut();
        }
      }
    })
  }
</script>