<h2><a href="/phpbasictemplate/country">Countries</a> / Create</h2>
<!--<a href="/phpbasictemplate/country">Back to List</a>-->
<form action="/phpbasictemplate/country/create" method="post" class="uk-form uk-form-horizontal">
  <div class="uk-form-row">
    <?=Html::LabelFor($model,'value')?>
    <div class="uk-form-controls">
      <?=Html::EditorFor($model, 'value')?>
      <?=Html::ValidationFor($model, 'value')?>
    </div>
  </div> 
  <div class="uk-form-row">
    <?=Html::LabelFor($model,'description')?>
    <div class="uk-form-controls">
      <?=Html::EditorFor($model, 'description')?>
      <?=Html::ValidationFor($model, 'description')?>
    </div>
  </div> 
    <button type='submit' class="uk-button uk-button-success">Save</button>
</form>