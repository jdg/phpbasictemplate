<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <title>Project Name - <?=$_pagetitle?> </title>
    <link rel="stylesheet" href="/phpbasictemplate/public/css/uikit.min.css">
    <!--<link rel="stylesheet" href="/phpbasictemplate/public/css/uikit.docs.min.css">-->
    <link rel="stylesheet" href="/phpbasictemplate/public/css/app.css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,300' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="/phpbasictemplate/public/js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="/phpbasictemplate/public/js/uikit.min.js"></script>
    
  </head>
  <body class="sidebar-active">
    <div id="ui-blocker">
      <!--<div class="ui-block-content">
        <div class="ui-block-spinner-container">
          <i class="uk-icon-spinner uk-icon-spin"></i>  
        </div>
      </div>-->
    </div>
    <?php
        if($_sidebar)
        {
            include("sidebar.php");
            echo "<script>
              let sidebar = window.sessionStorage.getItem('sidebar');
              console.log(sidebar);
              if(sidebar != null)
              {
                document.body.className = sidebar;
              }
            </script>";
        }
    ?>
    <div class='main'>
      <nav class="uk-navbar appheader">
          <div class="uk-navbar-content"><button id="hamburger" class="uk-navbar-toggle"></div>
          <!--<a href="" class="uk-navbar-toggle">e</a>-->
          <div class="uk-navbar-content uk-navbar-flip">
            <a href="#" id="signout"><i class="uk-icon-sign-out"></i>Sign out</a>
            <!--<div data-uk-dropdown="{mode: 'click'}">
              <div><a href="#"><i class="uk-icon-cog"></a></div>
              <div class="uk-dropdown">
                <ul class="uk-nav uk-nav-dropdown">
                  <li><a href="#">Exit</a></li>
                </ul>
              </div>-->
            <!--</div>-->
          </div>
          <div class="uk-navbar-content uk-navbar-center">Inventory System</div>
      </nav>
      <div class='maincontent'>
        <?php include($_htmlsections["body"]); ?>  
      </div>
    </div>
  </body>
  <script>
      
      $(window).load(function(){
        $("#ui-blocker").fadeOut("slow");
      });
      $("#hamburger").click(function(){
        toggleSideBar();
      });
      
      function toggleSideBar()
      {
        $("body").toggleClass("sidebar-active");
        if(sessionStorage.getItem('sidebar') != 'sidebar-active')
        {
          sessionStorage.setItem('sidebar','sidebar-active');
        }
        else {
          sessionStorage.setItem('sidebar','') ;
        }
      }
      
      $(document).ready(function(){
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
          toggleFullScreen();
          $("body").toggleClass("sidebar-active");
        }
      });
      
      function toggleFullScreen() {
        var doc = window.document;
        var docEl = doc.documentElement;

        var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
        var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

        if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
          requestFullScreen.call(docEl);
        }
        else {
          cancelFullScreen.call(doc);
        }
      }
    </script>
</html>