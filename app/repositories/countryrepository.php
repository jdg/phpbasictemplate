<?php
    class CountryRepository implements ICountryRepository{
        
        private $dbcontext;
        public function __construct($dbcontext)
        {
          $this->dbcontext = $dbcontext;
        }
        
        public function GetAll()
        {
          return Country::all();   
        }
        
        public function Create(Country $country)
        {
          $this->dbcontext->Add($country);
        }
        
        public function Delete(Country $country)
        {
          $this->dbcontext->Remove($country);
        }
        
        public function Update(Country $country)
        {
          $this->dbcontext->Update($country);
        }
    }
    
    interface ICountryRepository{
        public function GetAll();
        // public function FindById($id);
        public function Create(Country $country);
        public function Update(Country $country);
        public function Delete(Country $country);
    }