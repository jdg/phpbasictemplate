1. always use require_once '';
2. always use <?=$model->property?> in views when displaying data

3. whenever you want to display something to the browser that came from the user input
   always use
   echo htmlspecialchars($string, ENT_QUOTES, 'UTF-8');

4. always use prepared statements

5. in displaying dates, use format mm/dd/yyyy

security links:
1. https://www.owasp.org/index.php/PHP_Security_Cheat_Sheet